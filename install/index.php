<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Rekuz\Lessphp;

Loc::loadMessages(__FILE__);

if (class_exists('rekuz_lessphp')) {
    return;
}

class rekuz_lessphp extends CModule
{
    /** @var string */
    public $MODULE_ID;

    /** @var string */
    public $MODULE_VERSION;

    /** @var string */
    public $MODULE_VERSION_DATE;

    /** @var string */
    public $MODULE_NAME;

    /** @var string */
    public $MODULE_DESCRIPTION;

    /** @var string */
    public $MODULE_GROUP_RIGHTS;

    /** @var string */
    public $PARTNER_NAME;

    /** @var string */
    public $PARTNER_URI;

    public function __construct()
    {
        $this->MODULE_ID = 'rekuz.lessphp';
        $this->MODULE_VERSION = '1.0';
        $this->MODULE_VERSION_DATE = '2015-06-05 11:49:23';
        $this->MODULE_NAME = Loc::getMessage('BX_LESS_PHP_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('BX_LESS_PHP_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = "Rekuz";
        $this->PARTNER_URI = "http://pr.rekuz.ru";
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
    }

    public function DoUninstall()
    {
        ModuleManager::unregisterModule($this->MODULE_ID);
    }

}
