<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Loader::registerAutoLoadClasses('rekuz.lessphp', array(
    'Rekuz\Lessphp' => 'lib/Classes/Lessphp.php',
));

/*
EventManager::getInstance()->addEventHandler(
    'main',
    'OnBeforeEndBufferContent',
    array('Rekuz\Lessphp', 'reloadLessOnBeforeEndBufferContent')
);

EventManager::getInstance()->addEventHandler(
    'main',
    'OnEndBufferContent',
    array('Rekuz\Lessphp', 'clearLessOnEndBufferContent')
);
*/