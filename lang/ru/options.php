<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['REFERENCES_ON'] = 'Включить модуль';
$MESS['MAIN_CTRL_TAB_SET'] = 'Опции';
$MESS['MAIN_CTRL_TAB_TITLE_SET'] = 'Настройка модуля';
$MESS['REFERENCES_OPTIONS_RESTORED'] = "Восстановлены настройки по умолчанию";
$MESS['REFERENCES_OPTIONS_SAVED'] = "Настройки сохранены";
$MESS['REFERENCES_INVALID_VALUE'] = "Введено неверное значение";
$MESS['MAIN_SAVE'] = 'Применить';
$MESS['MAIN_OPT_SAVE_TITLE'] = 'Применить изменения';
$MESS['MAIN_HINT_RESTORE_DEFAULTS'] = 'Установить значения по умолчанию';
$MESS['MAIN_HINT_RESTORE_DEFAULTS_WARNING'] = 'Внимание! Все настройки будут перезаписаны значениями по умолчанию. Продолжить?';
$MESS['MAIN_RESTORE_DEFAULTS'] = 'По умолчанию';