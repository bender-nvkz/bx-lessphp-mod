<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['BX_LESS_PHP_MODULE_NAME'] = 'Битрикс-Less.php';
$MESS['BX_LESS_PHP_MODULE_DESCRIPTION'] = 'Модуль поддержки LESS формата для таблиц стилей';