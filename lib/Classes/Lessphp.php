<?php
namespace Rekuz;
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;
//use Bitrix\Main\Config\Option;
use Bitrix\Main\CAllMain;

Loc::loadMessages(__FILE__);

class Lessphp
{

    private static $_instance = null;

    private $_arLess = array();

    private function __construct() {
        require_once (dirname(__FILE__))."/less.php/Less.php";
    }
    protected function __clone() { }

    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function clearLessOnEndBufferContent(&$content)
    {
        $lessngn = self::getInstance();
        echo'<pre>';
        print_r($lessngn->_arLess);
        echo'</pre>';

        /*
        $arPatternsToRemove = Array(
            '/<script.+?src=".+?kernel_main\/kernel_main\.js\?\d+"><\/script\>/',
            '/<script.+?src=".+?bitrix\/js[^"]+"><\/script\>/',
            '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/script>/',
            '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/script>/',
            '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/script>/',
            '/<script.+?>\nbxSession\.Expand\(.+?\).*?\n<\/script>/',
            '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
            '/<link.+?href=".+?bitrix\/js\/main\/core\/css\/core[^"]+"[^>]+>/',

            //'/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/styles.css[^"]+"[^>]+>/',
            //'/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/template_styles.css[^"]+"[^>]+>/',
        );

        $content = preg_replace($arPatternsToRemove, "", $content);

        $content = preg_replace("/\n{2,}/", "\n\n", $content);
        */
    }

    public static function reloadLessOnBeforeEndBufferContent()
    {
        $lessngn = self::getInstance();
        $io = \CBXVirtualIo::GetInstance();
        $arLessFiles = array();
        foreach($lessngn->_arLess as $lessFile)
        {
            $arLessFiles[$_SERVER['DOCUMENT_ROOT'].key($lessFile)] = dirname(key($lessFile)).'/';
        }
        $cache_rel_dir = $io->CombinePath(BX_PERSONAL_ROOT."/cache/less", SITE_ID);
        $cache_abs_dir = $io->RelativeToAbsolutePath($cache_rel_dir);
        if(!$io->DirectoryExists($cache_abs_dir))
            $io->CreateDirectory($cache_abs_dir);
        $css_file_name = \Less_Cache::Get(
            $arLessFiles,
            array(
                'cache_dir' => $cache_abs_dir,
                'prefix' => SITE_ID."_"
            )
        );
        \Bitrix\Main\Page\Asset::getInstance()->addCss($cache_rel_dir."/".$css_file_name);
    }

    public function addLess($file,$cache_subdir)
    {
        global $APPLICATION;
        $io = \CBXVirtualIo::GetInstance();
        if($file && $io->FileExists($_SERVER['DOCUMENT_ROOT'].$file)) {
            try {
                $lessngn = self::getInstance();
                $cache_rel_dir = BX_PERSONAL_ROOT."/cache/less";
                if($cache_subdir && ($io->ValidatePathString("/".$cache_subdir) || $io->ValidatePathString($cache_subdir)))
                    $cache_rel_dir = $io->CombinePath($cache_rel_dir, $cache_subdir);
                $cache_abs_dir = $io->RelativeToAbsolutePath($cache_rel_dir);
                if(!$io->DirectoryExists($cache_abs_dir))
                    $io->CreateDirectory($cache_abs_dir);
                $file_path = pathinfo($file);
                $css_file_name = \Less_Cache::Get(
                    array( $_SERVER['DOCUMENT_ROOT'].$file => dirname($file).'/' ),
                    array(
                        'cache_dir' => $cache_abs_dir,
                        'prefix' => $file_path['filename']."_"
                    )
                );
                \Bitrix\Main\Page\Asset::getInstance()->addCss($cache_rel_dir."/".$css_file_name);
                $lessngn->_arLess[] = array($file => $css_file_name);
            } catch (Exception $e) {
                $APPLICATION->ThrowException($e->getMessage());
            }
            return;
        }
    }

    public function AddTemplate()
    {
        $siteTpl = $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/header.php';
        $backtrace = debug_backtrace();
        $callFile = $backtrace[0]['file'];
        $tplDir = dirname($backtrace[0]['file']);
        $cache_dir = substr_replace($tplDir, '', 0, strlen($_SERVER['DOCUMENT_ROOT']));
        if($callFile == $siteTpl) {
            $style_less = substr_replace($tplDir, '', 0, strlen($_SERVER['DOCUMENT_ROOT']))."/template_styles.less";
        } else {
            $style_less = substr_replace($tplDir, '', 0, strlen($_SERVER['DOCUMENT_ROOT']))."/style.less";
        }
        self::addLess($style_less,$cache_dir);
    }
}
